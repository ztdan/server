#!/usr/bin/env ruby
#
# Author:: Lance Albertson <lance@osuosl.org>
# Copyright:: Copyright 2020, Cinc Project
# License:: Apache License, Version 2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Update version tag to use stable/cinc naming scheme we use
o = "chef-server/omnibus_overrides.rb"
file = File.read(o)
regex = /(override :chef, version: )"(v[0-9].+)"/

replace = file.gsub(regex, '\1"stable/cinc-\2"')
File.open(o, "w") { |f| f.puts replace }
